---
title: "How to run several hydrus1d models in batch mode"
output: html_notebook
---

This is a 2020 update of the original file describing how to run Hydrus model in batch mode. In this example, we demonstrate how to run several instance of Hydrus while varying the Ks-value.

# Prerequires

1. You must have clone or downloaded a version of the tw-modeling-toolkit and set it as your working directory
1. You must have created a reference model using the GUI. Create a **permanent model** within a folder located in your User directory. For this example, the reference model has already been created. Simply unzip the folder example1. (If you are using git, add the freshly unzipped folder to the .gitignore file)
2. Make sure to uncheck the box *Hit Enter at the End?* located in the *Print Information* window.
3. Copy the appropriate .exe file from *Program Files* directory (Hydrus is usually installed within a folder named *PC-progress*) to your model folder.

For example, if you want to run an equilibrium 1D model, you must copy the file named *H1D_Calc.exe*.

To avoid the usage of local path ans setwd() command, we use the library here (install.packages("here")). See <https://github.com/jennybc/here_here> for more information.
```{r}
library(here)
```

# load all necessary functions and define your main directory

To load all necessary functions, source these two files:
```{r}
source(here("hydrus","file_manip.R"))
source(here("hydrus","simulation_fcn.R"))
```


The main directory is where you saved your model and where a new folder will be created for each run you will set up. In this example, we will use the example1 folder that has been created after unzipping the archive. However, it is a best practice to use a folder placed outside the tw-modeling-toolkit folder, especially if you are using git.
```{r}
main.directory <- here("hydrus","example1")
```

Indicate the number of run to be carried out:
```{r}
nb.runs <- 2
```

As explained earlier, each simulation results will be placed in a new folder. It's name will be composed of a common root and the number of the run. The common root is named *generic.name*.
```{r}
generic.name <- "1d_example_"

```

Within the main directory, you must have placed a gui file ending with an extension *.h1d* and a folder containing all input and output files. The path of this folder must be saved within the variable *hydrus.folder*. With the number of runs, we can initialize the folder tree.
```{r}
hydrus.folder <- here("hydrus","example1","1DRAINAG")
for (i in 1:nb.runs){
  new.hydrus.subfolder(new.name = paste0(generic.name,i),
                       reference.folder = main.directory,
                       hydrus.folder = hydrus.folder)  
}
```

So far, the functionalities of the toolkit allow you to replace a line of a given input file using the function modify.inputs.

In this example we run a model with a single layer. The hydraulic conductivity of this layer is set in the file called "selector.in" at line 29 along with the other hydraulic parameters. Therefore you must create a two new lines with all the parameters including the new desired hydraulic conductivity.
Let first create a matrix with a number of line equal to the number of run (nrow=2) and the number of columns equal to the number of parameters (ncol=6).
```{r}
material.hydraulic.properties <- matrix(c(0, 0.3308, 0.01433, 1.506, 25, 0.5, 0, 0.3308, 0.01433, 1.506, 50, 0.5),nrow=2,byrow=TRUE)
print(material.hydraulic.properties)
```
The fifth column correspond to the saturated hydraulic conductivity. It is set to 25 [cm/hour] in the first run (initial value of the model) and to 50 [cm/hour] in the second run.

Then each line of the matrix is converted to a string and substitute to the original parameters in the selector.in file of each run subfolder.
```{r}
for (i in 1:nb.runs){
  modify.inputs(input.file.path = paste0(hydrus.folder,"/selector.in"),
                line.number = 29,
                new.file.path = here("hydrus","example1",paste0(generic.name,i),"selector.in"),
                new.line = gsub(",", " ", toString(material.hydraulic.properties[i,])))
}
```

Next we need to create the level_01 file for each run subfolders.
```{r}
for (i in 1:nb.runs){
  level_01.creator(here("hydrus","example1",paste0(generic.name,i)))
}
```

Finally, we run the simulations using the function *simulation.run*. This function output the computing time:
```{r eval=FALSE, include=FALSE}
output <- simulation.run(nb.runs,main_directory,generic.name,hydrus.calc="H1D_Calc.exe")
print(output)
```

