# Nicolas Forquet, 2015

# new.hydrus.subfolder = function(new.name,reference.folder,hydrus.folder.path){
#   # new.name : name of the new hydrus subfolder
#   # reference.folder : folder containing all new hydrus subfolder
#   # hydrus.folder : folder containing the reference model obtained with the GUI
#   # NOTE: the reference folder must contain a copy of the Hydrus batch executable file : H2D_Calc.exe
#   
#   # get the content list from the reference folder
#   files2copy <- list.files(path = hydrus.folder.path)
#   # create a new folder into the reference folder
#   setwd(reference.folder)
#   dir.create(new.name)
#   # copy all input files
#   file.copy(list.files(pattern = ".exe", ignore.case = TRUE),paste0(new.name,"/",list.files(pattern = ".exe", ignore.case = TRUE)))
#   for (i in 1:length(files2copy)){
#     file.copy(paste(hydrus.folder,files2copy[i],sep = "/"),paste(new.name,files2copy[i],sep = "/"))
#   }
# }

dummy.white.space = function(char.line){
  temp = unlist(strsplit(char.line," ",fixed = TRUE))
  test.temp = (temp =="")
  result.vector = temp[!test.temp]
  return(result.vector)
}

read.meshtria = function(reference.folder){
  setwd(reference.folder)
  conn = file("meshtria.txt",open="r")
  linn = readLines(conn)
  close(conn)
  
  
  nb.nodes = as.numeric(dummy.white.space(linn[1])[2])
  nb.mesh = as.numeric(dummy.white.space(linn[1])[4])
  coord.nodes = data.frame(x = rep(0,nb.nodes), z = rep(0,nb.nodes))
  edges = data.frame(i = rep(0,nb.mesh), j = rep(0,nb.mesh), k = rep(0, nb.mesh))
  
  for (i in 1:nb.nodes){
    coord.nodes$x[i] = as.numeric(dummy.white.space(linn[i+1])[2])
    coord.nodes$y[i] = as.numeric(dummy.white.space(linn[i+1])[3])
  }
  end.of.table = i+1
  for (m in 1:nb.mesh){
    edges$i[m] = as.numeric(dummy.white.space(linn[m+end.of.table+3])[2])
    edges$j[m] = as.numeric(dummy.white.space(linn[m+end.of.table+3])[3])
    edges$k[m] = as.numeric(dummy.white.space(linn[m+end.of.table+3])[4])
  }
  
  output = list(nb.nodes = nb.nodes, nb.mesh = nb.mesh, coord.nodes = coord.nodes, edges = edges, linn = linn)
  return(output)
}

write_domain = function(reference.folder, file.name.out = "empty", Axz = "empty", Bxz = "empty", nb.col = 11, header.size = 6){
  # set working directory to reference folder
  setwd(reference.folder)
  # Hydrus has been developed with Windows. This is bad. What is even worse is that Windows is not case sensitive and therefore Simunek sometimes names its file in upper case, sometimes in lower case. Therefore I introduced a step that list the files having the name we are looking for independly of the case
  file.name.in <- list.files(pattern = "domain.dat", ignore.case = TRUE)
  
  # Then we open the file and extract the lines content and put in linn.
  conn <- file(file.name.in,open="r")
  linn <- readLines(conn)
  close(conn) 
  
  # test on model arguments
  if ((is.character(Axz) == TRUE) & (is.character(Bxz) == TRUE)){
    stop("At least Axz or Bxz must be given")
  }
  
  Axz.log <- is.character(Axz)
  Bxz.log <- is.character(Bxz)
  if (Axz.log == FALSE){
    nb.nodes <- length(Axz)
  } else {
    nb.nodes <- length(Bxz)  
  }
   
  # prealocation of matrix that contains data from the domain.dat file
  node.mat = matrix(rep(0,nb.col*nb.nodes),ncol=nb.col)
  
  # filling up node.map line by line starting after the header.size
  for (i in 1:nb.nodes){
    node.mat[i,] = as.numeric(dummy.white.space(linn[header.size+i]))
  }
  
  if (Axz.log == FALSE){
    node.mat[,7] = Axz
  }
  if (Bxz.log == FALSE){
    node.mat[,8] = Bxz  
  }
  
  new_linn = linn
  for (i in 1:nb.nodes){
    new_linn[i+header.size] = paste(as.character(node.mat[i,]),collapse=" ")
  }
  
  new_linn[(header.size+nb.nodes+1):length(linn)] = linn[(header.size+nb.nodes+1):length(linn)]
  
  if (file.name.out == "empty"){
    file.name.out <- file.name.in
  }
  
  conn = file(file.name.out,open="w")
  writeLines(new_linn,conn)
  close(conn) 
  
  return(new_linn)
}

level_01.creator = function(new.subfolder.path, HP1 = FALSE){
  # new.subfolder.path : the path to which create the level_01.dir file (and the path.dat file if HP1 is in use)
  file.create(paste(new.subfolder.path,"/Level_01.dir",sep=""))
  level01.file = file(paste(new.subfolder.path,"/Level_01.dir",sep=""),"w")
  writeLines(new.subfolder.path,level01.file)
  close(level01.file)
  if (HP1 == TRUE){
    file.create(paste(new.subfolder.path,"/path.dat",sep=""))
    path.file = file(paste(new.subfolder.path,"/path.dat",sep=""),"w")
    writeLines(new.subfolder.path,path.file)
    close(path.file)
  }
}

exe_copy = function(hydrus.path, new.subfolder.path, HP1 = FALSE){
  path <- paste0(hydrus.path, "/H1D_CALC.EXE")
  new.path <- paste0(new.subfolder.path, "/H1D_CALC.EXE")
  file_copy(path, new.path, overwrite = TRUE)
  if (HP1 == TRUE){
    path <- paste0(hydrus.path, "/HP1.exe")
    new.path <- paste0(new.subfolder.path, "/HP1.exe")
    file_copy(path, new.path, overwrite = TRUE)
    path <- paste0(hydrus.path, "/Phreeqc.dll")
    new.path <- paste0(new.subfolder.path, "/Phreeqc.dll")
    file_copy(path, new.path, overwrite = TRUE)
  }
}

modify.selector = function(new.name,hydrus.folder,data,header.size = 24, end.size = 65){
  # nombre de materiaux dans le modele
  nb.mat <-dim(data)[1]
  #read old file to get data written before and after the material matrix
  selector.file = file(paste(hydrus.folder,"/SELECTOR.IN",sep=""),"r")
  #store the header.size first lines (no transformation will be done on those lines)
  before.text = readLines(selector.file,n=header.size)
  # read data with material properties (data not save)
  readLines(selector.file,n=dim(data)[1])
  # store file content after material properties
  after.text = readLines(selector.file,n=end.size)
  #test.data = list(before = before.text,mat1 = mat1.text,mid = mid.text, mat2 = mat2.text, after = after.text)
  close(selector.file)
  
  #create the new selector.in in the appropriate folder
  selector.new.file = file(paste(new.name,"/SELECTOR.IN",sep=""),"w")
  # write text block before material properties
  writeLines(before.text,selector.new.file)
  # enter the new material properties
  write.table(data,selector.new.file,row.names=FALSE,col.names=FALSE,sep="\t")
  # write text block after material properties
  writeLines(after.text,selector.new.file)
  # close file connection
  close(selector.new.file)
}

modify.hydraulic.parameters = function(new.name,hydrus.folder,data,hydrus.version="1D"){
  # so far this function as only be tested for hydrus 1D
  #find the header line number corresponding to hydraulic parameters
  selector.file <- file(paste(hydrus.folder,"/SELECTOR.IN",sep=""),"r")
  file.content <- readLines(selector.file,n=-1)
  nb.line.start <- which(grepl("thr",file.content))
  close(selector.file)
  # number of model materials
  nb.mat <-dim(data)[1]
  #store the lines before the hydraulic parameters block (no transformation will be done on those lines)
  before.text = file.content[1:nb.line.start]
  #store the lines after the hydraulic parameters block (no transformation will be done on those lines)
  after.text = file.content[nb.line.start+nb.mat+1:length(file.content)]
  #create the new selector.in in the appropriate folder
  selector.new.file = file(paste(new.name,"/SELECTOR.IN",sep=""),"w")
  # write text block before material properties
  writeLines(before.text,selector.new.file)
  # enter the new material properties
  write.table(data,selector.new.file,row.names=FALSE,col.names=FALSE,sep="\t")
  # write text block after material properties
  writeLines(after.text,selector.new.file)
  # close file connection
  close(selector.new.file)
}

read.binary = function(filename,nb.elts,nb.ts){
  fileSize <- file.info(filename)$size
  raw <- readBin(filename,what="raw",n=fileSize)
  if (length(raw) %% 4 !=0){
    stop('File format assumption error')
  }
  nbrOfRecords <- length(raw)/4
  unsort.data = readBin(con=raw,what="double",size = 4,n = nbrOfRecords,endian="little")
  time.ts = rep(0,nb.ts)
  data.ts = matrix(rep(0,(nb.ts*nb.elts)),ncol = nb.ts)
  for(i in 1:nb.ts){
    time.ts[i] = unsort.data[i+nb.elts*(i-1)]
    data.ts[1:nb.elts,i] = unsort.data[(nb.elts*(i-1)+i+1):(i*(nb.elts+1))]
  }
  output = list(time.ts = time.ts, data.ts = data.ts)
  return(output)
}

modify.inputs <- function(input.file.path, line.number, new.file.path, new.line){
  # read.existing.file
  con <- file(input.file.path)
  file.content <- readLines(con, n=-1)
  close(con)
  file.before <- file.content[1:line.number-1]
  file.after <- file.content[line.number+1:length(file.content)]
  new.file <- file(new.file.path, "w")
  writeLines(file.before, new.file)
  writeLines(new.line, new.file)
  writeLines(file.after, new.file)
  close(new.file)
  return(file.after)
}

modify.input <- function(path, working.folder){
  # path: the path to the parameter json file (see parameter.file.creator)
  
  # load necessary librairies
  library(rjson)
  # read the json parameter file
  parameter <- fromJSON(file = path)
  # the number of simulations is the length of the parameter list
  nb.simul <- length(parameter)
  # iterate on the number of simulations
  for (i in 1:nb.simul){
    # path to the file to be modified
    input.file.path <- paste0(working.folder,"//","simulation","_",as.character(i),"//",parameter[[i]]$file.name)
    # open the connection
    con <- file(input.file.path)
    # read all file lines
    file.content <- readLines(con, n=-1)
    # close the connection
    close(con)
    # lines that remained unchanged before the line to be modified
    file.before <- file.content[1:parameter[[i]]$line.number-1]
    # lines that remained unchanged after the line to be modified
    file.after <- file.content[parameter[[i]]$line.number+1:length(file.content)]
    # open connection again
    con <- file(input.file.path, "w")
    # write lines that remained unchanged before
    writeLines(file.before, con)
    # write the line that has been modified
    writeLines(parameter[[i]]$new.line, con)
    # write lines that remained unchanged after
    writeLines(file.after, con)
    # close connection
    close(con)
  }
}

parameter.file.creator <- function(path, reference.model.folder){
  # path: the path to the json parameter file to be created
  # reference.model.folder: the reference.model.folder containing the orginal input files
  # load necesary libraries
  library(rjson)
  library(tidyverse)
  # nb.simul: number of simulation
  nb.simul <- parse_integer(readline(prompt = "Enter the number of simulations: "))
  # check input format
  if(is.na(nb.simul) == TRUE){
    stop("invalid format. The number of simulations must be an integer")
  }
  # create an empty list of size nb.simul
  output <- vector(mode = "list", length = nb.simul)
  # nb.line2modif: number of lines to be modified
  nb.line2modif <- parse_integer(readline(prompt = "Enter the number of lines in the input files that are modified: "))
  # check input format
  if(is.na(nb.line2modif) == TRUE){
    stop("invalid format. The number of lines to modify must be an integer")
  }
  # create empty data.frame for each simulation
  for (i in 1:nb.simul){
    output[[i]] <- data.frame(file.name = rep(0,nb.line2modif), line.number = rep(0,nb.line2modif), new.line = rep(0,nb.line2modif))
  }
  for (i in 1:nb.line2modif){
    # display the progress in input collection
    print(paste0("line to modify ",i,"/",nb.line2modif))
    # temp: temporary variable storing the name of the file to be modified
    temp <- readline(prompt = "Enter the name of the file to be modified: ")
    # check if the file exists
    if (file.exists(path = paste0(reference.model.folder,"//",temp)) == FALSE){
      stop("invalid file name")
    }
    # fill up the output list
    for (j in 1:nb.simul){
      output[[j]]$file.name[i] <- temp
    }
    rm(temp)
    # temp: temporary variable storing the number of the line to be modified in the input file
    temp <- parse_integer(readline(prompt = "Enter the number of the line to be modified in the input file: "))
    # check input format
    if(is.na(temp) == TRUE){
      stop("invalid format. The number of the line to be modified must be an integer")
    }
    # fill up the output list
    for (j in 1:nb.simul){
      output[[j]]$line.number[i] <- temp
    }
    rm(temp)
    # print the explanation on how the line must be splited
    writeLines("A line contains strings that will not be modified and strings that will be. \n Strings that will not be modified and that are juxtaposed should be considered as one block. Strings that will be modified should be considered as independent blocks. \n Example : block 1(the name of the variable), block 2(50), block 3(the unit of the variable)")
    # nb.blocks: number of blocks in the line
    nb.blocks <- parse_integer(readline(prompt = "Enter the number of blocks in the line to be modified: "))
    # check input format
    if(is.na(nb.blocks) == TRUE){
      stop("invalid format. The number of blocks must be an integer")
    }
    # create the data.frame blocks that temporarily stores block information
    blocks <- data.frame(matrix(0,nb.simul,nb.blocks))
    for (j in 1:nb.blocks){
      # display progress in block filling
      print(paste0("block n°",j,"/",nb.blocks))
      # is the current block invariant ?
      invariant <- parse_logical(readline(prompt = "Is this block invariant? (TRUE or FALSE): "))
      # check variable format
      if(is.na(invariant) == TRUE){
        stop("invalid format. Invariant must be a logical")
      }
      # if the block is invariant, its content is similar for all simulations
      if (invariant == TRUE){
        blocks[,j] <- readline(prompt = "Enter the content of the block: ")
        
      } else {
      # if the block varies, its value for each simulation must be provided
        for (k in 1:nb.simul){
          blocks[k,j] <- readline(prompt = paste0("Enter the content of the block for simulation", k ,": "))
        }
      }
    }
    # blocks information are concatenated
    for (j in 1:nb.simul){
      output[[j]]$new.line <- paste0(blocks[j,],collapse = " ")
    }
  }
  # the list is transform into a json and the output file is written
  json <- toJSON(output)
  write(json, file = path)
  
  # I left the function output for verification purposes
  return(output)
}
